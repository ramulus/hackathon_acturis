import { Component, Injectable } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http'
import { identifierModuleUrl } from '@angular/compiler';
import { Level1DataService } from './../level1Data.service';

@Component({
  selector: 'app-level1',
  templateUrl: './level1.component.html',
  styleUrls: ['./level1.component.css']
})

@Injectable()
export class Level1 {
  title = 'ryjbook';
  question : Object;
  teams: Array<any>;
  score: number;

constructor (private httpClient : Http, private level1DataService: Level1DataService) {
  this.getData()
  this.score = 0
}

makeAnswer(answer, image){
  return { "answerID": answer, "imageId": image }
}
  
getData() {
  return this.httpClient.get("https://h4ctur1s.firebaseapp.com/data.json")
  .toPromise().then(response => 
    { 
      this.teams = response.json().Teams;
      this.question = this.level1DataService.getLevel1Data(this.teams);
    });
}

check(imageId: number, nameId: number) {

  if(imageId == nameId) {
    this.score = this.score + 1
    this.getData();
    return;
  }
  this.score = this.score - 2
  this.getData()
}
}

