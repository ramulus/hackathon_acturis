export class Level2DataService {

    getLevel2Data(teamsData)
    {
      var level2Data: any = {};
    
      var flattenedMemberList = this.flattenMembersData(teamsData);
    
      var randomImages = this.selectRandomImages(flattenedMemberList, 3);
      var winningMemberId = randomImages[Math.floor(Math.random() * randomImages.length)].id;
      
      level2Data.answers = randomImages;
      level2Data.question = this.selectWinningMember(flattenedMemberList, winningMemberId);
    
      return level2Data;
    }
    
    private selectWinningMember(flattenedMembersList, winningMemberId)
    {
      for(let member of flattenedMembersList)
      {
        if(member.id === winningMemberId)
        {
          var winningImage: any = {};
          winningImage.id = winningMemberId;
          winningImage.name = member.name;
    
          return winningImage;
        }
      }
    }
    
    private selectRandomImages(memberList, randomImageCount)
    {
      var memberList = memberList.slice();
      var randomizedImageArray = [];
    
      var shuffledMemberList = this.shuffle(memberList);
    
      for(var i = 0; i < randomImageCount; i++)
      {
        var randomImage: any = {};
    
        var randomMemberData = shuffledMemberList.pop();
    
        randomImage.id = randomMemberData.id;
        randomImage.name = randomMemberData.image;
    
        randomizedImageArray.push(randomImage);
      }
    
      return randomizedImageArray;
    }
    
    private flattenMembersData(teamsData)
    {
      var counter = 0;
      var members = [];
    
      for(let team of teamsData)
      {
        for(let member of team.Members)
        {
          let newMemberData: any = {};
          
          newMemberData.team = team.Name;
          newMemberData.name = member.Name;
          newMemberData.image = member.Image;
          newMemberData.id = counter
    
          members.push(newMemberData);
    
          counter++;
        }
      }
      
      return members;
    }
    
    shuffle(a) {
      var j, x, i;
      for (i = a.length - 1; i > 0; i--) {
          j = Math.floor(Math.random() * (i + 1));
          x = a[i];
          a[i] = a[j];
          a[j] = x;
      }
      return a;
    }
    }