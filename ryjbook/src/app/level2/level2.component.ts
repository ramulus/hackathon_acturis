import { Component, Injectable } from '@angular/core';
import {Http, Headers, RequestOptions } from '@angular/http'
import { identifierModuleUrl } from '@angular/compiler';
import { Level2DataService } from './../level2Data.service';

@Component({
  selector: 'app-level2',
  templateUrl: './level2.component.html',
  styleUrls: ['./level2.component.css']
})

@Injectable()
export class Level2 {
    question : Object;
    teams: Array<any>;
    score: number = 0;
    max: number = 4;

constructor (private httpClient : Http, private level2DataService: Level2DataService) 
{
    this.getData()   
}

getData() {
    return this.httpClient.get("https://h4ctur1s.firebaseapp.com/data.json")
    .toPromise().then(response => 
      { 
        this.teams = response.json().Teams;
        this.question = this.level2DataService.getLevel2Data(this.teams);
        console.log(this.question)
      });
  }


  check(imageId: number, nameId: number) {

    if(imageId == nameId) {
      this.score = this.score + 1
    }
    else {
      this.score = this.score - 2
    }
    this.getData()
  }

}