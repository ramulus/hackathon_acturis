import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { Level1 } from './level1/level1.component'
import { Level2 } from './level2/level2.component'

const routes: Routes = [
  { path: '', redirectTo: '/level1', pathMatch: 'full' },
  {
    path: 'level1',
    component: Level1,
  },
  {
    path: 'level2',
    component: Level2,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
