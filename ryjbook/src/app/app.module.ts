import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Level1DataService } from './level1Data.service';
import { Level2DataService } from './level2Data.service';
import { HttpModule } from '@angular/http';

import { Level1 } from './level1/level1.component';
import { Level2 } from './level2/level2.component';

@NgModule({
  declarations: [
    AppComponent,
    Level1,
    Level2
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    Level1DataService,
    Level2DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
