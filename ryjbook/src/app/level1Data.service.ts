export class Level1DataService {

getLevel1Data(teamsData)
{
  var level1Data: any = {};

  var flattenedMemberList = this.flattenMembersData(teamsData);

  var randomMembers = this.selectRandomMembers(flattenedMemberList, 3);
  var winningImageId = randomMembers[Math.floor(Math.random() * randomMembers.length)].id;
  level1Data.answers = randomMembers;
  level1Data.image = this.selectWinningImage(flattenedMemberList, winningImageId);

  return level1Data;
}

private selectWinningImage(flattenedMembersList, winningImageId)
{
  for(let member of flattenedMembersList)
  {
    if(member.id === winningImageId)
    {
      var winningImage: any = {};
      winningImage.id = winningImageId;
      winningImage.filename = member.image;

      return winningImage;
    }
  }
}

private selectRandomMembers(memberList, randomMemberCount)
{
  var memberList = memberList.slice();
  var randomizedMemberArray = [];

  var shuffledMemberList = this.shuffle(memberList);

  for(var i = 0; i < randomMemberCount; i++)
  {
    var randomMember: any = {};

    var randomMemberData = shuffledMemberList.pop();

    randomMember.id = randomMemberData.id;
    randomMember.name = randomMemberData.name;
    randomMember.team = randomMemberData.team;

    randomizedMemberArray.push(randomMember);
  }

  return randomizedMemberArray;
}

private flattenMembersData(teamsData)
{
  var counter = 0;
  var members = [];

  for(let team of teamsData)
  {
    for(let member of team.Members)
    {
      let newMemberData: any = {};
      
      newMemberData.team = team.Name;
      newMemberData.name = member.Name;
      newMemberData.image = member.Image;
      newMemberData.id = counter

      members.push(newMemberData);

      counter++;
    }
  }
  
  return members;
}

shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}
}